nettoe (1.5.1-4) UNRELEASED; urgency=medium

  * Remove constraints unnecessary since buster (oldstable):
    + Build-Depends: Drop versioned constraint on dpkg-dev.

 -- Debian Janitor <janitor@jelmer.uk>  Sun, 23 Oct 2022 12:40:26 -0000

nettoe (1.5.1-3) unstable; urgency=medium

  * Team upload.
  * Switch to debhelper-compat = 13.
  * Remove dh-autoreconf and autopoint from B-D.
  * Declare compliance with Debian Policy 4.5.0.
  * Move nettoe to salsa.debian.org.
  * Remove obsolete NEWS file.
  * Let dh_auto_configure pass --host to ./configure.
    Thanks to Helmut Grohne for the patch. (Closes: #922561)
  * Work around FTBFS with GCC 10 by building with -fcommon. (Closes: #957602)

 -- Markus Koschany <apo@debian.org>  Sun, 20 Sep 2020 15:51:53 +0200

nettoe (1.5.1-2) unstable; urgency=low

  * Set Standards-Version to 3.9.7, no changes.
  * debian/control: Give a secured Vcs-Browser target URL.
  * debian/nettoe.menu: Removed, since the upstream source builds
    and installs its own desktop file, which is now preferred.

 -- Mats Erik Andersson <mats.andersson@gisladisker.se>  Tue, 05 Apr 2016 22:37:31 +0200

nettoe (1.5.1-1) unstable; urgency=low

  * New upstream release.
    + This resolves our problem when linking against libncurses.
  * NMU is acknowledged, but its solution is made obsolete by new release.
  * Adapt to upstream's new intention of signing the release.
    + debian/upstream/signing-key.asc: New file, public key DA109DAF.
    + debian/watch: Updated.

 -- Mats Erik Andersson <mats.andersson@gisladisker.se>  Thu, 24 Apr 2014 03:29:56 +0200

nettoe (1.5-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * If TERM isn't defined, pass --without-terminfo to configure
    Patch by Dejan Latinovic
    Closes: #736481

 -- Anibal Monsalve Salazar <anibal@debian.org>  Tue, 25 Mar 2014 12:38:31 +0000

nettoe (1.5-1) unstable; urgency=low

  * New upstream release.
  * Standard 3.9.5; no change needed.
  * debian/control: Add libncurses5-dev as build dependency.
  * debian/nettoe.desktop, debian/nettoe.install:
    No longer needed, so removed.

 -- Mats Erik Andersson <mats.andersson@gisladisker.se>  Fri, 10 Jan 2014 12:39:43 +0100

nettoe (1.4.2-1) unstable; urgency=low

  * New upstream release. (Closes: #709447)
  * Follow standard 3.9.4:
    + debian/control: Update standards field.
  * debian/rules: Reimplement using 'dh'.
    + debian/compat: Set to 9.
    + debian/control: Build depends on debhelper (>= 9),
        dh-autoreconf, autopoint, dpkg-dev (>= 1.16.1~).
  * debian/control: Update Vcs-Svn and Vcs-Browser.
  * debian/copyright: Conform to specification of version 1.0.
  * [lintian] Trivial manpage issue:
    + debian/patches/01_manpage.diff: New file.
  * Desktop integration.  Thanks to Markus Koschany. (Closes: #709448)
    + debian/nettoe.menu: Update with icon location.
    + debian/nettoe.desktop: New file.
    + debian/nettoe.install: New file.
    + debian/icons/16x16/nettoe.png, debian/icons/32x32/nettoe.png,
      debian/nettoe.xpm: New files, reduced from larger original
        "http://nettoe.sourceforge.net/images/nettoe_big.gif".
    + debian/source/include-binaries: New file.

 -- Mats Erik Andersson <mats.andersson@gisladisker.se>  Sat, 29 Jun 2013 13:17:04 +0200

nettoe (1.3.2-1) unstable; urgency=low

  * New upstream release.
    + debian/patches/01-listen_socket.diff: Not needed, removed.
    + debian/patches/series: Emptied.

 -- Mats Erik Andersson <mats.andersson@gisladisker.se>  Wed, 16 Jun 2010 22:22:26 +0200

nettoe (1.3.1-2) unstable; urgency=low

  * debian/patches/01-listen_socket.diff: New file.
    + Workaround for different behaviour for glibc-2.7 and eglibc-2.10.

 -- Mats Erik Andersson <mats.andersson@gisladisker.se>  Thu, 11 Mar 2010 21:17:33 +0100

nettoe (1.3.1-1) UNRELEASED; urgency=low

  * New upstream release.
    + IPv6-able code, input sanitation.
  * Source format "3.0 (quilt)".
  * Standards 3.8.4, no changes.
  * debian/NEWS: New file.
  * debian/copyright: Set the pointer to 'common-licenses/GPL-2'.

 -- Mats Erik Andersson <mats.andersson@gisladisker.se>  Fri, 26 Feb 2010 15:55:06 +0100

nettoe (1.2.1-1) unstable; urgency=low

  [ Mats Erik Andersson }
  * Formulate debian/copyright according to DEP-5.
  * Move to Standards 3.8.3, and debhelper (>= 7.0), compatibility 7.
    + debian/rules: use 'dh_prep' instead of 'dh_clean -k'.
  * debian/control:
    + Add M E Andersson as uploader, keeping Debian Games Team as
      proper maintainer. (Closes: #544916)
    + Short description: use an initial small case letter.
    + Add ${misc:Depends} to binary package section Depends.
  * Set the suite to 'unstable', ready to release.
  * debian/rules: Simplify some targets to use 'dh $@'.

  [ Barry deFreese ]
  * Update my e-mail addresses.

 -- Mats Erik Andersson <mats.andersson@gisladisker.se>  Thu, 14 Jan 2010 09:58:22 +0100

nettoe (1.1.0-4) UNRELEASED; urgency=low

  [ Barry deFreese ]
  * Remove XS prefix from VCS fields

  [ Eddy Petrișor ]
  * fix Vcs-Svn URL to point to svn, not the incomplete ssh location

 -- Eddy Petrișor <eddy.petrisor@gmail.com>  Wed, 05 Mar 2008 02:01:34 +0200

nettoe (1.1.0-3) unstable; urgency=low

  [ Barry deFreese ]
  * New maintainer (Closes: #274026)
    + Debian Games Team <pkg-games-devel@lists.alioth.debian.org>
  * Add myself to uploaders
  * Bump compat/debhelper build-dep to 5
    + Version debhelper build-dep
  * Fix watch file (Closes: #450166)
  * Update package description (Closes: #348003)
  * Bump standards version to 3.7.3 (No changes needed)
  * Add VCS and Homepage fields in control

 -- Barry deFreese <bddebian@comcast.net>  Mon, 10 Dec 2007 13:23:19 -0500

nettoe (1.1.0-2) unstable; urgency=low

  * QA upload.
  * Package is orphaned (see #268125); set maintainer to Debian QA Group.
  * src/network.c: Set SO_REUSEADDR to avoid `Address already in use'
    errors when restarting.  Closes: #227641.
  * Switch to debhelper 4.
  * debian/changelog: Remove obsolete Emacs local variables.
  * debian/menu: Add quotes to placate Lintian.
  * debian/rules: Honor DEB_BUILD_OPTIONS=noopt.
  * Conforms to Standards version 3.6.2.

 -- Matej Vela <vela@debian.org>  Fri, 21 Oct 2005 11:58:40 +0200

nettoe (1.1.0-1) unstable; urgency=low

  * New upstream release.

 -- Andras Bali <bali@debian.org>  Mon,  6 Aug 2001 22:22:47 +0200

nettoe (1.0.6-1) unstable; urgency=low

  * New upstream release.

 -- Andras Bali <bali@debian.org>  Sun,  1 Jul 2001 19:58:16 +0200

nettoe (1.0.5-1) unstable; urgency=low

  * Initial Release (Closes: #96202).

 -- Andras Bali <bali@debian.org>  Sat, 23 Jun 2001 22:24:49 +0200
